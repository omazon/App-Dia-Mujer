<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
require app_path().'/Includes/phpimage/phpimage.php';
use PHPImage;


class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/');
//        $this->multi_line('test.jpg',strtoupper('Que las mujeres comencemos a darnos a respetar y valorarse a sí misma'));
//        return view('sub.share');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'body' =>'required|max:83',
        ]);
        $message = new Message;
        $message->body = $request->body;
        $message->slug = $this->generateRandomString(25);
        $message->url_image = $this->generateRandomString(15).'.jpg';
        $this->multi_line($message->url_image,$message->body);
        $message->save();
        alert()->success('Solo queda que compartas tu mensaje con todos tus amigos','Gracias por tu mensaje')->autoclose(5000);
        return redirect('/'.$message->slug);
    }
    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function multi_line($slug,$body){
        $bg = 'storage/default.jpg';
        $image = new PHPImage();
        $image->setDimensionsFromImage($bg);
        $image->draw($bg);
        $image->setFont('storage/EBRIMABD.TTF');
        $image->setTextColor(array(0,0,0));
        $image->setStrokeColor(array(0, 0, 0));
        $image->setAlignHorizontal('center');
        $image->textBox('"'.strtoupper($body).'"', array(
            'width' => 900,
            'height' => 600,
            'fontSize' => 36,
            'x' => 550,
            'y' => 450,
        ));
        $image->setTextColor(array(0,0,0));
        $image->save('storage/'.$slug);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
