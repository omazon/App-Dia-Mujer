<link rel="icon" href="{{asset('img/favicon.png')}}">
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/main.css')}}">
<link rel="stylesheet" href="{{asset('sweetalert/sweetalert.css')}}">
<link rel="stylesheet" href="{{asset('plugin/owlcarousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('plugin/owlcarousel/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">