@extends('main')
@section('title','Comparte tu publicación')
<meta property="fb:app_id" content="279294285839032" />
<meta property="og:type" content="article">
<meta property="og:title" content="#LoQueNosFaltaEs">
<meta property="og:description" content="{{$message->body}}">
<meta property="og:url" content="{{env('APP_URL')}}/{{$message->slug}}">
<meta property="og:image" content="{{env('APP_URL')}}/storage/{{$message->url_image}}">
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="#LoQueNosFaltaEs">
<meta name="twitter:description" content="{{$message->body}}">
<meta name="twitter:image" content="{{env('APP_URL')}}/storage/{{$message->url_image}}">
<meta name="twitter:image:alt" content="#LoQueNosFaltaEs">

@section('content')
    <section id="share-section">
    <img class="img-responsive share-image center-block" src="storage/{{$message->url_image}}" alt="">
        <div class="btn-redes">
            <a class="btn btn-info" href="storage/{{$message->url_image}}" download>
                <i class="fa fa-download"></i> Descargar imagen</a>
            <a class="btn btn-facebook" href="https://www.facebook.com/dialog/share?app_id=279294285839032&display=popup&href=http://www.loquenosfalta.es/{{$message->slug}}&redirect_uri=http://www.loquenosfalta.es/{{$message->slug}}">
                <i class="fa fa-twitter"></i> Facebook</a>
            <a class="btn btn-twitter" href="https://twitter.com/intent/tweet?text={{$message->body}}&url=http://www.loquenosfalta.es/{{$message->slug}}&hashtags=loquenosfaltaes">
                <i class="fa fa-facebook-f"></i> Twitter</a>
            <a class="btn btn-primary" href="/">
                <i class="fa fa-chevron-left"></i> Regresar</a>
        </div>
    </section>
@endsection