@extends('main')
@section('title','#LoQueNosFaltaEs')
@section('redes')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="fb:app_id" content="279294285839032" />
    <meta property="og:type" content="article">
    <meta property="og:title" content="#LoQueNosFaltaEs">
    <meta property="og:description" content="Dejemos de ver el 8 de Marzo como un día para celebrar y utilicemos esta fecha para crear conciencia sobre los derechos que aún no tienen las mujeres nicaragüenses y del mundo.">
    <meta property="og:url" content="{{env('APP_URL')}}">
    <meta property="og:image" content="{{env('APP_URL')}}/img/Fondo_Compartir_Generica.jpg">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="#LoQueNosFaltaEs">
    <meta name="twitter:description" content="Dejemos de ver el 8 de Marzo como un día para celebrar y utilicemos esta fecha para crear conciencia sobre los derechos que aún no tienen las mujeres nicaragüenses y del mundo.">
    <meta name="twitter:image" content="{{env('APP_URL')}}/img/Fondo_Compartir_Generica.jpg">
    <meta name="twitter:image:alt" content="#LoQueNosFaltaEs">
    @endsection
@section('content')
    <header>
        <img class="img-responsive left-top" src="{{asset('img/Logo.png')}}" alt="">
        <img class="img-responsive center-center" src="{{asset('img/Frase_BannerPrincipal.png')}}" alt="">
        <a href="#problema" class="center-bottom ">
            <img class="element-animation" src="{{asset('img/Flecha_Scrolldown.png')}}" alt="">
        </a>
    </header>
    <section id="problema">
        <img class="img-responsive center-block" src="{{asset('img/Porque.jpg')}}" alt="">
        <div class="text1 container">
            <p>Dejemos de ver el 8 de Marzo como un día para celebrar y utilicemos esta fecha para crear conciencia sobre los derechos que aún no tienen las mujeres nicaragüenses y del mundo.</p>
            <p>Las personas ocupan los pocos derechos alcanzados por las mujeres, para justificar las injusticias que continúan viviendo: femicidios, violaciones, acoso, falta de oportunidades laborales, pago no igualitario, estereotipos y discriminación en la educación.
            </p>
        </div>
        <div class="text2 text-center">
            <p class="line1">LOS DATOS SON</p>
            <p class="line2">ALARMANTES</p>
        </div>
    </section>
    <section id="datos" class="owl-carousel owl-theme">
        <div class="article">
            <img src="{{('img/slide/1.png')}}" alt="">
        </div>
        <div class="article">
            <img src="{{('img/slide/2.png')}}" alt="">
        </div>
        <div class="article">
            <img src="{{('img/slide/3.png')}}" alt="">
        </div>
        <div class="article">
            <img src="{{('img/slide/4.png')}}" alt="">
        </div>
        <div class="article">
            <img src="{{('img/slide/6.png')}}" alt="">
        </div>
        <div class="article">
            <img src="{{('img/slide/7.png')}}" alt="">
        </div>
        <div class="article">
            <img src="{{('img/slide/8.png')}}" alt="">
        </div>
        <div class="article">
            <img src="{{('img/slide/9.png')}}" alt="">
        </div>
    </section>
    <section id="iniciativa">
        <div class="container">
            <h2>INICIATIVA</h2>
            <p>Creemos firmemente, que este tema tiene que ser parte de nuestras conversaciones diarias. Así como conversamos del clima, el tráfico, el fútbol, las clases etc. Por eso en el mes de marzo, Chureca Chic pone el tema sobre la mesa con nuestra campaña #LoQueNosFaltaEs.</p>
            <p> Creamos 8 tazas para conmemorar el día de la mujer, con mensajes positivos y reconociendo los logros que hemos alcanzado. Sin embargo, cuando llenás las tazas de café revelan un mensaje impactante que nos hace reflexionar sobre los logros que nos quedan por alcanzar.   </p>
        </div>
    </section>
    <section id="taza">
        <section class="owl-carousel owl-theme slide-taza">
            <div class="article">
                <img src="{{('img/taza/Mensaje1.png')}}" alt="">
            </div>
            <div class="article">
                <img src="{{('img/taza/Mensaje2.png')}}" alt="">
            </div>
            <div class="article">
                <img src="{{('img/taza/Mensaje3.png')}}" alt="">
            </div>
            <div class="article">
                <img src="{{('img/taza/Mensaje4.png')}}" alt="">
            </div>
            <div class="article">
                <img src="{{('img/taza/Mensaje6.png')}}" alt="">
            </div>
            <div class="article">
                <img src="{{('img/taza/Mensaje7.png')}}" alt="">
            </div>
            <div class="article">
                <img src="{{('img/taza/Mensaje8.png')}}" alt="">
            </div>
        </section>
    </section>
    <section id="chureca">
        <div class="container">
            <img class="img-responsive center-block" src="{{asset('img/Logo_ChurecaChic.jpg')}}" alt="">
            <div class="text-center somos-chureca">
                <p>Chureca Chic es el resultado del trabajo de la fundación Earth Education Project, que busca mejorar la calidad de vida de las mujeres nicaragüenses de la comunidad de La Chureca. Estas mujeres ingresan a un programa de becas que incluye un taller de reciclaje de papel, aprenden manualidades y elaboran bisutería, al tiempo que reciben educación básica, formación laboral y desarrollo psicosocial.</p>
                <p>Utilizando materiales reciclados como papel, cartón, tela y cuero, nuestras artesanas elaboran piezas de joyería única. La totalidad de las ganancias de Chureca Chic son destinadas a financiar parte de las becas de Earth Education Project. Sin embargo, todavía nos faltan muchas metas por alcanzar.</p>
            </div>
            </p>
            <h2 class="text-center">¿QUÉ NOS HACE FALTA?</h2>
            <div class="text-center">
                <div class="col-sm-4">
                    <h3>FORMACIÓN LABORAL</h3>
                    <p>Queremos lograr acceso a formación laboral gratis y efectiva para mujeres, para que puedan sacar a sus familias de la pobreza por medio de trabajos fijos y estables.</p>
                </div>
                <div class="col-sm-4">
                    <h3>RED DE RECICLAJE</h3>
                    <p>Necesitamos establecer una red de reciclaje formal en Nicaragua que ofrezca empleo digno y prevenga la contaminación del medio ambiente.</p>
                </div>
                <div class="col-sm-4">
                    <h3>MODELO A SEGUIR</h3>
                    <p>Queremos llegar a ser un modelo de referencia de la verdadera definición de emprendimiento social. Establecer una modelo que pueda ser replicado a nivel nacional e internacional.</p>
                </div>
            </div>
            </p>
        </div>
    </section>
    <section id="formulario">
        <div class="row">
            <div class="container text-center">
                <div class="line-height">
                    <h2>DECÍNOS QUE PENSAS</h2>
                    <p>Y COMPARTÍ EN TUS REDES</p>
                    <p>#LOQUENOSFALTAES</p>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="container form-container">
                {!! Form::open(['method'=>'POST','class'=>'form','route'=>'message.store']) !!}
                <div class="form-group">
                    {!! Form::textarea('body',null,['class'=>'form-control','placeholder'=>'Agrega tu mensaje','required'=>'required','max'=>83,'onkeyup'=>'textCounter(this,"counter",83)','id'=>'message']) !!}
                </div>
                <div class="form-group">
                    <input disabled  maxlength="3" size="3" value="83" id="counter">
                    <span>Máximo 83 caracteres</span>
                    {!! Form::submit('ENVIAR',['class'=>'btn pull-right']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    <footer>
        <span>2017 &copy; Todos los derechos reservados</span>
    </footer>
    <script>
        function textCounter(field,field2,maxlimit)
        {
            var countfield = document.getElementById(field2);
            if ( field.value.length > maxlimit ) {
                field.value = field.value.substring( 0, maxlimit );
                return false;
            } else {
                countfield.value = maxlimit - field.value.length;
            }
        }
    </script>
@endsection