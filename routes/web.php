<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Message;

Route::get('/', function () {
    return view('app');
})->name('home');

Route::resource('message','MessageController');
Route::get('/{url}',function($url){
    //    url activo
    $message = Message::where('slug',$url)->first();
    if($url == $message->slug){
        return view('sub.share')->with('message',$message);
    }
    else{
        return 'error 404';
    }

});
